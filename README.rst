Test RST file
=============

Let's see how it pans out.
What about equations: :math:`a_1=\sqrt{a}{b}`  ?
What if I try dollar signs: $a_1=\sqrt{a}{b}$ ?
And square brackets:  \[a_1=\sqrt{a}{b}\]